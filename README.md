# lambda-sorting
### Luis Guilherme Coelho <lgcoelho@disroot.org>

This project consist of some sorting utilities for Common Lisp written under the functional paradigm, developed for a college assignment about sorting algorithms.

## License
GPL-3.0-or-later