;;;; package.lisp

(defpackage #:lambda-sort
  (:use #:cl)
  (:export #:quicksort
	   #:mergesort
	   #:tr-quicksort
	   #:tr-mergesort
	   #:i-mergesort
	   #:rt-tr-mergesort
	   #:shellsort
	   #:heapsort
	   #:bogosort
	   #:order
	   #:disjoin-order
	   #:shuffle
	   #:swap
	   #:group
	   #:seq
	   #:start))
