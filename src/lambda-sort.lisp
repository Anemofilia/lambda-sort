
;;;; lambda-sort.lisp

(in-package #:lambda-sort)
(proclaim '(optimize speed))

;;; Start package
(defun start ()
  (mapc (lambda (x) (format t x))
	'("~%λ-Sort is free software, licensed under the GNU Public License~&"
	  "version 3 or later, and it comes with absolutely no warranty.~%~%"
	  "⠀⣤⣤⣶⣦⣤⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠸⠟⠛⠛⢿⣿⣷⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⠀⠀⠀⠀⠙⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀Welcome to λ-Sort!⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⠀⠀⠀⠀⠀⠸⣿⣿⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⠀⠀⠀⠀⠀⢰⣿⣿⣷⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⠀⠀⠀⠀⢠⣿⣿⢿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⠀⠀⠀⢠⣿⣿⠇⠈⣿⣿⣆⠀⠀⠀An awesome sorting tool written entirely⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⠀⠀⢀⣾⣿⡏⠀⠀⠸⣿⣿⡄⠀⠀⠀under the functional paradigm, in Common Lisp.⠀⠀~&"
	  "⠀⠀⠀⢀⣾⣿⡟⠀⠀⠀⠀⢻⣿⣷⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⠀⣼⣿⡿⠁⠀⠀⠀⠀⠈⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠀⣼⣿⣿⠃⠀⠀⠀⠀⠀⠀⠘⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⣸⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⢹⣿⣿⡄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~&"
	  "⠀⠉⠉⠉⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀~%~%"
	  "Type (help) for inline help and (q) to quit λ-Sort. Happy hacking! ~%"))
  (in-package #:lambda-sort))

;;; Quit package
(defun q ()
  "Quits from the λ-sort package to the #:CL-USER package."
  (princ "Thanks for using lambda-sort!")
  (in-package #:CL-USER))

;;; Help information
(defun help ()
  "Displays some useful information for the user. Returns nil."
  (mapc (lambda (x) (format t x)) '("~%Aplication options:~%"
			       "(sort-file <filename> <algorithm> <key> <property>)~%"
			       "> Creates a file within the path path/to/sorted-file~%"
			       "  containing the elements of the original file,~%"
			       "  sorted by the choosen key.~%~%"
			       "(q)~%"
			       "> Closes λ-Sorting.~%~%"))
  nil)

;;; Sorting data in a file
(defun get-file (filename)
  "Returns a list containing the lines of the given file."
  (with-open-file (stream filename)
    (labels ((read-recursively (read-so-far)
               (let ((line (read-line stream nil 'eof)))
                 (if (eq line 'eof)
                     (reverse read-so-far)
                     (read-recursively (cons line read-so-far))))))
      (read-recursively ()))))

(defun map-to-list (x)
  "Returns the corresponding list to the given string X where the
 spaces in the initial strings are read as element separators and
 the rest as proper elements."
  (declare (string x))
  (read-from-string (concatenate 'string "(" x ")")))
      
(defun map-to-string (x)
  "Returns the corresponding string to the given list X. "
  (declare (list x))
  (if (some #'atom x)
      (reduce (lambda (x y) (concatenate 'string x " " y))
	      (mapcar #'write-to-string x))
      (reduce (lambda (x y) (format nil "~a~%~a" x y))
	      (mapcar #'map-to-string x))))

(defun sort-file (filename algorithm predicate &key key)
  "Creates a file containing a rearrangement of the contents of the given file
ordered by the given PREDICATE and KEY."
  (with-open-file (outfile (concatenate 'string (directory-namestring filename)
					"sorted-" (pathname-name filename))
			   :direction :output
			   :if-exists :supersede
			   :if-does-not-exist :create)
    (labels ((g (x) (if (cdr x) (mapcar #'map-to-list x) (map-to-list (car x))))
	     (f (x) (funcall algorithm x predicate :key key)))
      ((lambda (file) (format outfile (map-to-string (f (g file)))))
       (get-file filename))))
  (format t "~%File sorted!"))

;;; Distribution information
(defun license ()
  "Displays the text of the Lambda-Sort package license."
  (mapc (lambda (x) (format t "~%~a" x))
	(get-file "~/quicklisp/local-projects/lambda-sort/license.org"))
  nil)

;; EOF
