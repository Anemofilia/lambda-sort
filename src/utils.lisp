;;;; utils.lisp

(in-package #:lambda-sort)
(proclaim '(optimize speed))

;;; Toolbox ;;;
(import 'alexandria:compose)

;;; Group
(defun group (group-length list &key (overlap 0))
  "Returns a list containing all groups of GROUP-LENGTH consecutive elements
in LIST with the given OVERLAP of the lists appended, when it exists, with the
part of list that couldn't form a group with the specified GROUP-LENGTH."
  (declare ((integer 1) group-length) (integer overlap) (list list))
  (labels ((make-groups (lst acc)
	     (let ((length (length lst)))
	       (cond ((< 0 length group-length) (reverse (cons lst acc)))
		     ((< length group-length) (reverse acc))
		     (t (make-groups (nthcdr (- group-length overlap) lst)
				     (cons (subseq lst 0 group-length) acc)))))))
    (make-groups list ())))
  
;;; Order
(defun order (predicate &optional fn)
  "Returns a compilled predicate function corresponding to the predicate-function
 order. if no FN is supplied, order returns PREDICATE."
  (declare (function predicate) ((or function null) fn))
  (if (null fn)
      predicate
      (alexandria:named-lambda predicate-fn-order (xi &rest args)
	(cond ((null args) t)
	      ((funcall predicate
			(funcall fn xi)
			(funcall fn (car args)))
	       (apply #'predicate-fn-order args))))))

(defun order-compose (predicate &rest more-predicates)
  "Returns a compilled predicate function corresponding to the composition of PREDICATE
and all other predicates in MORE-PREDICATES. The returned function returns t if its input
is ordered with respect to some of these keys."
  (declare (function predicate) (list more-predicates))
  (if more-predicates
      (apply #'order-compose
	     (alexandria:named-lambda composed-order (xi &rest args)
	       (cond ((null args) t)
		     ((funcall predicate xi (car args))
		      (apply #'composed-order args))
		     ((and (not (funcall predicate (car args) xi))
			   (funcall (car more-predicates) xi (car args)))
		      (apply #'composed-order args))))
	     (cdr more-predicates))
      predicate))


;;; Seq
(defun seq (start end &optional (rate (signum (- end start))))
  "Returns the list containing all elements in the arithmetical progression from
 START to END with the given RATE."
  (declare (real start end rate))
    (if (/= (signum rate) (signum (- end start)))
	(error "END can't be reached in a sequence with the given START and RATE."))
	(labels ((seq-aux (alpha acc)
	     (cond ((or (zerop rate) (zerop (- end start))) (list start))
		   ((or (and (plusp rate) (> alpha end)) (and (minusp rate) (< alpha end)))
		    (reverse acc))
		   (t (seq-aux (+ alpha rate) (cons alpha acc))))))
	  (seq-aux start ())))

(defun shuffle (list)
  "Returns a random rearrangement of LIST."
  (declare (list list))
  (labels ((shuffle-aux (xi acc)
	     (if (null xi) acc ((lambda (iota) (shuffle-aux
				       (append (subseq xi 0 iota) (subseq xi (1+ iota)))
				       (cons (nth iota xi) acc)))
			       (random (length xi))))))
    (shuffle-aux list ())))

;;; Swap
(defun swap (n m x)
  "Returns a list equal to `x', but with the nth and mth elements swapped."
  (declare (integer n m) (list x))
  (cond ((= n m) x)
	((< n m) (append (subseq x 0 n)
			 (cons (nth m x) (subseq x (1+ n) m))
			 (cons (nth n x) (subseq x (1+ m)))))
	(t (swap m n x))))

;;; EOF


