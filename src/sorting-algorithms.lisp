;;;; sorting-algorithms.lisp

(in-package #:lambda-sort)
(proclaim '(optimize speed))

;;; Quicksort (Divide and conquer)
(defun quicksort (list predicate &key key)
  "A purely functional, recursive implementation of quicksort. Returns a rearrangement
 of LIST, ordered with respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate) ((or function null) key))
  (if (cdr list)
      ((lambda (p) (append (quicksort (remove-if (complement p) (cdr list)) predicate :key key)
	       (cons (car list)
		     (quicksort (remove-if p (cdr list)) predicate :key key))))
       (lambda (xi) (funcall (order predicate key) xi (car list))))
      list))

;;; Mergesort (Divide and conquer)
(defun merge-lists (predicate l1 &optional (l2 ()) (acc ()))
  "Returns the append with ACC of a list containing all elements in L1 and L2,
 ordered by the given PREDICATE."
  (declare (list l1 l2 acc) (function predicate))
  (cond ((and l1 l2) (if (funcall predicate (car l1) (car l2))
		       (merge-lists predicate (cdr l1) l2 (cons (car l1) acc))
		       (merge-lists predicate (cdr l2) l1 (cons (car l2) acc))))
	(l1 (append (reverse acc) l1))
	(l2 (append (reverse acc) l2))))

(defun mergesort (list predicate &key key)
  "A purely functional, recursive implementation of mergesort. Returns a
 rearrangement of LIST, ordered with respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate) ((or function null) key))
  (if (cdr list)
      ((lambda (n) (merge-lists (order predicate key)
			   (mergesort (butlast list n) predicate :key key)
			   (mergesort    (last list n) predicate :key key)
			   ()))
       (floor (length list) 2))
      list))

;;; Quicksort (Tail recursive)
(defun particionate (list predicate)
  "Returns, with respect to the given PREDICATE, a partition list containing
 the list of elements suboptimal than (CAR LIST), a list containing the car and
 the list of elements superoptimal than the (CAR LIST)."
  (declare (list list) (function predicate))
  (cond ((cdr list) ((lambda (p) (list (remove-if (complement p) (cdr list))
			       (list (car list))
			       (remove-if p (cdr list))))
		  (lambda (xi) (funcall predicate xi (car list)))))
	(list (list list))
	(t list)))


(defun tr-quicksort (list predicate &key key internal-state)
  "A purely functional, tail recursive implementation of quicksort. Returns a
 rearrangement of LIST, ordered with respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate)
	   ((or function null) key) (boolean internal-state))
  (let ((order (order predicate key)))
    (cond ((and (null internal-state) (apply order list)) list)
	  ((null internal-state) (tr-quicksort (list list)
					       predicate
					       :key key
					       :internal-state t))
	  ((notany #'cdr list) (apply #'append list))
	  (t (tr-quicksort (mapcan (lambda (xi) (particionate xi order)) list)
			   predicate
			   :key key
			   :internal-state t)))))

;;; Mergesort (Purely Functional, Tail recursive)
(defun merge-once (list predicate)
  "Returns a list containing, for all elements with even index, the merge-lists
 of it and it's sucessor, when it exists, or the element itself."
  (declare (list list) (function predicate))
  (mapcar (lambda (xi) (apply (lambda (x &optional y) (merge-lists predicate x y)) xi))
	  (group 2 list)))

(defun tr-mergesort (list predicate &key key internal-state)
  "A purely functional tail recursive implementation of mergesort. Returns a
 rearrangement of LIST, ordered with respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate)
	   ((or function null) key) (boolean internal-state))
  (cond ((null internal-state)
	 (tr-mergesort (mapcar #'list list)
		       predicate
		       :key key
		       :internal-state t))
	((cdr list) ((lambda (order) (tr-mergesort (merge-once list order)
					   predicate
					   :key key
					   :internal-state t))
		  (order predicate key)))
	(t (car list)))) 

;;; Mergesort (Iterative)
(defun i-mergesort (list predicate &key key)
  "A purely functional iterative implementation of mergesort. Returns a 
rearrangement of LIST ordered with respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate)
	   ((or function null) key))
  (let ((order (order predicate key)))
    (labels ((iterate (fn n xi)
	       (if (zerop n) xi (iterate fn (1- n) (funcall fn xi)))))
      (car (iterate (lambda (xi) (merge-once xi order))
		    (ceiling (log (length list) 2))
		    (mapcar #'list list))))))

;;; Mergesort (Referentially Transparent, Tail Recursive)
(let ((internal-state nil))
  (defun rt-tr-mergesort (list predicate &key key)
    "A, referentially transparent, tail recursive implementation of mergesort.
 Returns a rearrangement of LIST, ordered with respect to the given PREDICATE and KEY."
    (declare (list list) (function predicate key))
    (cond ((null internal-state)
	   (setq internal-state t)
	   (rt-tr-mergesort (mapcar #'list list) predicate :key key))
	  ((null (cdr list)) (setq internal-state nil) (car list))
	  (t (rt-tr-mergesort (merge-once list (order predicate key))
			      predicate
			      :key key)))))

;;; Shellsort
(defun ciura (list)
  "Returns the Martin Ciura's gap sequence for LIST."
  (declare (list list))
  (labels ((ciura-aux (xi acc)
	     (cond ((> xi (car acc)) (ciura-aux xi (cons (floor (car acc) 4/9) acc)))
		   ((> xi (cadr acc)) (cdr acc))
		   (t (remove-if-not (lambda (zeta) (< zeta xi)) acc)))))
   (ciura-aux (length list) '(1750 701 301 132 57 23 10 4 1))))

(defun h-sort (list h predicate)
  "Returns a rearrangement of LIST such that, seeing the output as H interleaved
 lists starting anywhere, taking every hth element produces a sorted list."
  (labels ((h-sort-aux (xi i j)
	     (cond ((< (- (length xi) h) j) xi)
		   ((< i h) (h-sort-aux xi (- (length xi) j) (1+ j)))
		   ((funcall predicate (nth i xi) (nth (- i h) xi))
		    (h-sort-aux (swap (- i h) i xi) (1- i) j))
		   (t (h-sort-aux xi (1- i) j)))))
    (h-sort-aux list (1- (length list)) 1)))

(defun shellsort (list predicate &key key)
  "A purely functional, applicative implementation of shellsort. Returns a
rearrangement of LIST, ordered with respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate) ((or function null) key))
  ((lambda (order) (reduce (lambda (xi eta) (h-sort xi eta order)) (ciura list) :initial-value list))
   (order predicate key)))

;;; Heapsort
(defun heapify (list heapsize node predicate)
  "Returns the subtree with root at NODE into a heap with respect to the given
 PREDICATE."
  (declare (list list) (integer node heapsize) (function predicate))
  (let* ((left  (+ (* 2 node) 1))
	 (right (+ (* 2 node) 2))
	 (suboptimal (cond ((< right heapsize)
			    (alexandria:extremum (list node left right)
						 (complement predicate)
						 :key (lambda (n) (nth n list))))
			   ((< left  heapsize)
			    (alexandria:extremum (list node left)
						 (complement predicate)
						 :key (lambda (n) (nth n list))))
			   (t node))))
    (cond ((= node suboptimal) list)
	  (t (heapify (swap node suboptimal list) heapsize suboptimal predicate)))))

(defun build-heap (list heapsize predicate)
  "Returns a heap, with respect to the given PREDICATE, constituted from the
 elements in LIST."
  (declare (list list) (integer heapsize) (function predicate))
  (reduce (lambda (xi iota) (heapify xi heapsize iota predicate))
	  (seq (1- (floor heapsize 2)) 0)
	  :initial-value list))

(defun heapsort (list predicate &key key)
  "A purely functional, applicative implementation of heapsort. Returns a
 rearrangement of LIST, ordered with respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate) ((or function null) predicate))
  (let ((heapsize (length list))
	(order (order predicate key)))
    (reduce (lambda (xi eta) (heapify (swap 0 eta xi) eta 0 order))
	    (seq (1- heapsize) 0)
	    :initial-value (build-heap list heapsize order))))

;;; Bogosort
(defun bogosort (list predicate &key key)
  "A, just for sake of the meme, purely inefficient, tail recursive
 implementation of bogosort. Returns a rearrangement of LIST, ordered with
 respect to the given PREDICATE and KEY."
  (declare (list list) (function predicate) ((or function null) key))
  (if (apply (order predicate key) list)
      list
      (bogosort (shuffle list) predicate :key key)))

;; EOF
