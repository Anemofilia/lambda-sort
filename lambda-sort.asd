;;;; lambda-sort.asd

(asdf:defsystem #:lambda-sort
  :description "lambda-sort is a package of sorting tools written under the functional paradigm."
  :author "Luis Guilherme Coelho <lgcoelho@disroot.org>"
  :license  "GPLv3-or-later"
  :version "0.0.1"
  :pathname "src"
  :depends-on (#:alexandria)
  :serial t
  :components ((:file "package")
	       (:file "utils")
	       (:file "sorting-algorithms")
               (:file "lambda-sort")))
